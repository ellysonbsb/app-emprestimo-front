# Aplicação de empréstimo frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.

## Descrição

Trata-se de uma aplicação que realiza operações de empréstimo com juros simples.

## Rodando a aplicação

Para instalação de todos os pacotes dependentes:

`npm install`

O comando para inicializar a aplicação `ng serve`. Navegue para `http://localhost:4200/`.