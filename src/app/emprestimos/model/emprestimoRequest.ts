export interface EmprestimoRequest {
    nome: string;
    valorDesejado: number;
    numeroParcela: number;
}