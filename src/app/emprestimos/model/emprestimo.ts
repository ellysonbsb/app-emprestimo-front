export interface Emprestimo {
    id: string;
    nome: string;
    valorOriginal: number;
    valorParcela: number;
    numeroParcela: number;
    valorTotal: number;
}
