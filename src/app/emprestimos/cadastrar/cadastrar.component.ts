import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { EmprestimoService } from '../services/emprestimo.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Emprestimo } from '../model/emprestimo';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.scss']
})
export class CadastrarComponent implements OnInit {

  form: FormGroup;  
  isSimulacao : boolean = false;

  constructor(private location: Location, private emprestimoService : EmprestimoService, private formBuilder: FormBuilder,
    private snackBar: MatSnackBar) {
    this.form = this.formBuilder.group({
      nome: [null],
      numeroParcela: [null],
      valorDesejado: [null],
      valorParcela: [{value: "",disabled:true}],
      valorTotal: [{value: "",disabled:true}]
    });
  }

  ngOnInit(): void {
  }

  onSimular() {
    this.emprestimoService.simular(this.form.value)
    .subscribe({next : this.onSuccessSimular.bind(this), error : this.onError.bind(this)});
  }
  
  private onSuccessSimular(emprestimo: Emprestimo) {
    this.form.get('nome')?.disable();
    this.form.get('numeroParcela')?.disable();
    this.form.get('valorDesejado')?.disable();
    this.isSimulacao = true;
    this.form.patchValue({
      valorParcela: emprestimo.valorParcela,
      valorTotal: emprestimo.valorTotal
    });
  }

  onSubmit() {
    this.emprestimoService.salvar(this.form.value)
      .subscribe({next : this.onSuccess.bind(this), error : this.onError.bind(this)});
  }

  onCancelar() {
    this.location.back();
  }

  private onSuccess() {
    this.snackBar.open('Empréstimo salvo com sucesso!', '', { duration: 5000 });
    this.onCancelar();
  }

  private onError() {
    this.snackBar.open('Erro ao salvar empréstimo.', '', { duration: 5000 });
  }
  onLimpar() {
    this.form.get('nome')?.enable();
    this.form.get('numeroParcela')?.enable();
    this.form.get('valorDesejado')?.enable();
    this.isSimulacao = false;
    this.form.patchValue({
      valorParcela: 0,
      valorTotal: 0,
      nome: null,
      numeroParcela: null,
      valorDesejado: null
    });

  }
}
