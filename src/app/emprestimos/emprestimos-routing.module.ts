import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmprestimoComponent } from './emprestimo/emprestimo.component';
import {CadastrarComponent } from './cadastrar/cadastrar.component';

const routes: Routes = [
    { path : '', component: EmprestimoComponent},
    { path: 'simular', component: CadastrarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmprestimosRoutingModule { }
