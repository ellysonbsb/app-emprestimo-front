import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Emprestimo } from '../model/emprestimo';
import { first, tap } from 'rxjs/operators';
import { EmprestimoRequest } from '../model/emprestimoRequest';

@Injectable({
  providedIn: 'root'
})
export class EmprestimoService {

  private readonly API = 'http://localhost:8080/api/v1/emprestimos';
  constructor( private httpClient : HttpClient) { }

  list() {
    return this.httpClient.get<Emprestimo[]>(this.API)
    .pipe(
      first(),
      tap(empre => console.log(empre))
    );
  }
  
  simular(emprestimo: EmprestimoRequest) {
    return this.httpClient.post<Emprestimo>(this.API+'/simular', emprestimo).pipe(first());
  }

  salvar(emprestimo: EmprestimoRequest) {
    return this.httpClient.post<Emprestimo>(this.API, emprestimo).pipe(first());
  }
  
  excluir(id: number) {
    console.log('akiiiiii')
    return this.httpClient.delete(this.API+'/'+id).pipe(first());
  }
}
