import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmprestimosRoutingModule } from './emprestimos-routing.module';
import { EmprestimoComponent } from './emprestimo/emprestimo.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { CurrencyMaskModule } from "ng2-currency-mask";
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    EmprestimoComponent,
    CadastrarComponent
  ],
  imports: [
    CommonModule,
    EmprestimosRoutingModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    MatIconModule
  ]
})
export class EmprestimosModule { }
