import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Emprestimo } from '../model/emprestimo';
import { EmprestimoService } from '../services/emprestimo.service';

@Component({
  selector: 'app-emprestimo',
  templateUrl: './emprestimo.component.html',
  styleUrls: ['./emprestimo.component.scss']
})
export class EmprestimoComponent implements OnInit {

  emprestimos: Observable<Emprestimo[]>;
  displayedColumns = ['id', 'nome', 'valorOriginal', 'valorParcela', 'numeroParcela', 'valorTotal', 'actions'];

  constructor(private emprestimoService : EmprestimoService, private router: Router,
    private route: ActivatedRoute,private snackBar: MatSnackBar) {
    this.emprestimos = emprestimoService.list();
    
   }

  ngOnInit(): void {
  }
  simular() {
    this.router.navigate(['simular'], {relativeTo: this.route});
  }

  onCancelar(id: number) {
    this.emprestimoService.excluir(id)
    .subscribe({next : this.onSuccess.bind(this), error : this.onError.bind(this)});
  }
  
  private onSuccess() {
    this.snackBar.open('Empréstimo cancelado!', '', { duration: 5000 });
    this.emprestimos = this.emprestimoService.list();
  }

  private onError() {
    this.snackBar.open('Erro ao cancelar empréstimo.', '', { duration: 5000 });
  }

}
