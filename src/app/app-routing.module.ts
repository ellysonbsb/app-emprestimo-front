import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'emprestimos' },
  {
    path: 'emprestimos',
    loadChildren: () => import('./emprestimos/emprestimos.module').then(m => m.EmprestimosModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
